
# Update latest package on the system 
$ sudo yum update -y

# Install Apache Web Server
$ sudo yum install -y httpd.x86_64

# Start Apache Server
$ sudo systemctl start httpd.service

# Configure Apache to run on system boot
$ sudo systemctl enable httpd.service

# Get some HTML content 

# Move file into correct location:
$ sudo mv ~/redhat_bashscript.sh/index.html /var/www/html/index.html

# Replace the generic HTML page with our HTML file

